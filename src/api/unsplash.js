import axios from 'axios';  

export default axios.create({
  baseURL: 'https://api.unsplash.com',
  headers: {
    Authorization: 'Client-ID cf37ffe3817498e25bb03befdadba199c1118344d29e75887e8818c72a852755',
  }
})